import numpy as np
import scipy as sp
import pandas as pd
#import sys
import datetime
import math

#form a array to store vessel numbers
nod = pd.read_csv('G:\\centerlines\\nodes.csv') 
ele = pd.read_csv('G:\\centerlines\\elements.csv')
seqdata = pd.read_csv('G:\\centerlines\\sequence_data.csv')
#seqdatatuple = seqdata.iloc[:,0:2]
elearray = np.empty(0,dtype=int) #initiate a array to store vessel no.
element_id_all = ele.iloc[:,1 ] #dataframe of element_id in elements.csv
element_id_all_np = np.array(element_id_all)#turn dataframe into np array
nombre = element_id_all_np.shape[0]#how many vessels
for value in range(nombre-1):
    if element_id_all_np[value+1] - element_id_all_np[value] != 0: 
        elearray = np.append(elearray,element_id_all_np[value])
elearray = np.append(elearray,element_id_all_np[-1])

#print elearray.size,"vessels in total"
#for count in range(elearray.size):
    #print "{},".format(elearray[count]),

#get points and radius of each vessel

for ids in elearray:
    eleNo = ids
    lineinseqdata = seqdata.loc[seqdata["element_id"]==eleNo]
    lineinseqdata = lineinseqdata.dropna(axis=1)
    lineinseqdataNp = np.array(lineinseqdata,dtype=int)
    subnod=pd.DataFrame()
    num = lineinseqdataNp.flat[1]
    for count in xrange(lineinseqdataNp.flat[1]):
        lineofsubnod = nod.loc[nod["node_id"]==lineinseqdataNp.flat[2+count]]
        subnod = pd.concat([subnod,lineofsubnod])
    #print subnod

    coordinate = np.array(subnod[['x','y','z']])
    radius = np.array(subnod[['hoge3']])
    attribute = np.array(subnod[['hoge2']])
    #radiusMean = radius.mean()
    #subnod.to_csv("g://expe//subnod{}.csv".format(ids))

    with open("g:\\experiments6\\VesNo-{}.vtk".format(eleNo),"w") as vtkFile:
        vtkFile.write("# vtk DataFile Version 2.0\n")
        vtkFile.write("Vessel No.{}\n".format(eleNo))
        vtkFile.write("ASCII\n")
        vtkFile.write("DATASET POLYDATA\n")
        vtkFile.write("POINTS {} float\n".format(num))
        for i in xrange(num):
            #vtkFile.write("{} {} {}\n".format(coordinate[i,0]+108,coordinate[i,1]+115,coordinate[i,2]+604)) #to move a cerebral
            vtkFile.write("{} {} {}\n".format(coordinate[i,0],coordinate[i,1],coordinate[i,2]))
        vtkFile.write("LINES 1 {}\n".format(num+1))
        vtkFile.write("{} ".format(num))
        for k in range(num):
            vtkFile.write("{} ".format(k))
        vtkFile.write("\n")
        #print radius
        vtkFile.write("POINT_DATA {}\n".format(num))
        vtkFile.write("SCALARS MaximumInscribedSphereRadius float\n")
        vtkFile.write("LOOKUP_TABLE default\n")
        for j in xrange(num):
            vtkFile.write("{}\n".format(radius[j,0]))
        #print m_attibute
        #vtkFile.write("POINT_DATA {}\n".format(num))
        vtkFile.write("SCALARS m_attribute int\n")
        vtkFile.write("LOOKUP_TABLE default\n")
        for m in xrange(num):
            vtkFile.write("{}\n".format(attribute[m,0]))

        print "Output No.{} vessel at".format(eleNo),datetime.datetime.now()
